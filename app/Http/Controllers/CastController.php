<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    //
    public function create()
    {
        # code...
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast/create');
    }

    public function index()
    {
        # code...
        $listCast = DB::table('cast')->get();
        return view('cast.index', compact('listCast'));
    }

    public function show($id)
    {
        # code...
        $dataCast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('dataCast'));
    }

    public function edit($id)
    {
        # code...
        $editCast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('editCast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio'  => 'required'
        ]);
        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function delete($id)
    {
        # code...
        $deleteData = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
