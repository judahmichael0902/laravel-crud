@extends('master')
@section('title')
    Edit Cast {{$editCast->id}}
@endsection

@section('content')
<form action="/cast/{{$editCast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama">Edit Cast</label>
        <input type="text" class="form-control" name="nama" value="{{$editCast->nama}}" id="nama" placeholder="Masukkan Title">
        @error('nama')

            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$editCast->umur}}" id="umur" placeholder="Masukkan Body">
        @error('umur')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Biodata</label>
        <input type="text" class="form-control" name="bio" value="{{$editCast->bio}}" id="bio" placeholder="Masukkan Body">
        @error('bio')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection